/**
 * Minecraft Version: *Info in chat parser*
 *
 * ChatParser Version: 1.47, 1.47 (Test)
 *
 * If you are using a newer version of ChatParser but still using outdated methods from an old version,
 * it may either throw an error or nothing happened.
 * 
 * Example: 
 *   Old: playerChat.message
 *   New: playerChat.withColor.message
 * 
 * In version 1.47 and newer, playerChat.message no longer exists.
 *
 * I guess:
 *   - Kick parser
 * 	 - Should i make lightweight version of chatparser?
 *   - This is last chatparser of this version 1.20.4, if nothing wrong.
 */

const mc = require('minecraft-protocol');
const ChatParse = require("./chatparser.js");

function createBot() {

const bot = mc.createClient({
  // host: '95.216.192.50', // kaboom
  // host: 'chipmunk.land', // Chipmunk
  // host: '168.100.225.224', // Neko
  port: 25565,
  username: 'catparser', // cat
  version: "1.21.3", // 1.20.5, 1.20.6, Not support 1.21 or higher
});

ChatParse.inject(bot); // load catparser function

bot.on('error', (e) => { 
  if (bot !== undefined) bot.end(e.message);
});

bot.on('end', (reason) => {
    console.log(`Bot disconnect: ${reason}\nReconnect in 6 seconds...`);
	setTimeout(() => {
		createBot();
	}, 6e3);
    return;
});

bot.on('custom_playerChat', (message, playerChat, packet) => {
	console.log(`[PlayerChat] ${message}`);
	console.log(JSON.stringify(playerChat.jsonMsg.message))
});

bot.on('custom_profilelessChat', (message, profilelessChat, packet) => {
	console.log(`[ProfilelessChat] ${message}`);
});

bot.on('custom_systemChat', (message, systemChat, packet) => {
	console.log(`[SystemChat] ${message}`);
});

/*

// Kick message and disconnect message
bot.on('kick_disconnect', (kick) => {
  console.log(ChatParse.parseMinecraftMessage(kicked.reason));
})
bot.on('disconnect', (kick) => {
  console.log(ChatParse.parseMinecraftMessage(kicked.reason));
})
bot.on('custom_actionBar', (message, actionBar, packet) => {
	console.log(`[ActionBar] ${message}`);
});

*/

/*

bot.on('custom_bossBar', (message, bossBar, packet) => {
  switch (bossBar.action) {
    case 0:
	  console.log(`[BossBar Added] Name: ${bossBar.withColor.message}\nHealth: ${bossBar.health}\nColor: ${bossBar.color}\nDividers: ${bossBar.dividers}\nFlags: ${bossBar.flags}`);
	break;

	case 1:
	  console.log(`[BossBar Removed] ${bossBar.uuid}`);
	break;
	
	case 2:
	  console.log(`[BossBar Health] ${bossBar.health}`)
	break;

	case 3:
	  console.log(`[BossBar Message] ${bossBar.withColor.message}`)
	break;
	
	case 4:
	  console.log(`[BossBar Color] ${bossBar.color}`)
	  console.log(`[BossBar Style] ${bossBar.dividers}`)
	break;
	case 5:
      console.log(`[BossBar Flags] ${bossBar.flags}`)
	break;
  }
});

bot.on('custom_title', (message, title, packet) => {
	switch (title.type) {
	  case 0:
		console.log(`[Title] ${message}`)
	  break;
	  case 1:
		console.log(`[SubTitle] ${message}`)
	  break;
	  case 2:
		console.log(`[TitleSettings] fadeIn: ${title.fadeIn}\nstay: ${title.stay}\nfadeOut: ${title.fadeOut}`);
	  break;
	}
});

bot.on('custom_allMessage', (chatType, message, messagePacket, packet) => {
  switch (chatType) {
	case "playerChat":
	case "profilelessChat":
	case "actionBar":
		console.log(`[${chatType}] ${message}`)
	break;
	case "systemChat":
		if (messagePacket.jsonMsg?.message?.translate === "advMode.setCommand.success" || messagePacket.jsonMsg?.message?.translate === "multiplayer.message_not_delivered") return; // Command set: %s OR Can't deliver chat message, check server logs: %s
		console.log(`[${chatType}] ${message}`)
	break;
	case "bossBar": {
	  switch (messagePacket.action) {
		case 0:
		  console.log(`[BossBar Added] Name: ${messagePacket.withColor.message}\nHealth: ${messagePacket.health}\nColor: ${messagePacket.color}\nDividers: ${messagePacket.dividers}\nFlags: ${messagePacket.flags}`);
		break;

		case 1:
		  console.log(`[BossBar Removed] ${messagePacket.uuid}`);
		break;
	
		case 2:
		  console.log(`[BossBar Health] ${messagePacket.health}`)
		break;

		case 3:
		  console.log(`[BossBar Message] ${messagePacket.withColor.message}`)
		break;
	
		case 4:
		  console.log(`[BossBar Color] ${messagePacket.color}\n[messagePacket Style] ${messagePacket.dividers}`)
		break;
		case 5:
		  console.log(`[BossBar Flags] ${messagePacket.flags}`)
		break;
	  }
	}
	break;
	case "title": {
	  switch (messagePacket.type) {
	    case 0:
		  console.log(`[Title] ${message}`)
	    break;
	    case 1:
		  console.log(`[SubTitle] ${message}`)
	    break;
	    case 2:
		  console.log(`[TitleSettings] fadeIn: ${messagePacket.fadeIn}\nstay: ${messagePacket.stay}\nfadeOut: ${messagePacket.fadeOut}`);
	    break;
	  }
	}
	break;
  }
})
*/

}

createBot();